const express  =  require('express');
const app  =  express();
var path = require('path');
const config = require('./config')();

const port =  process.env.PORT || config.port;
const socketio_server = process.env.SOCKETIO_SERVER || config.server;

app.engine('html', require('ejs').renderFile);
app.use('/static', express.static('views'));

app.listen(port);

app.get('/', function (req, res) {
    res.render(__dirname + "/views/index.html", {socketio_server:socketio_server, forceid:false, doorid:null});
});

app.get('/token', function (req, res) {
    res.render(__dirname + "/views/index.html", {socketio_server:socketio_server, forceid:true, doorid:req.query.id});
});



console.log(`Server running on ${port} port, PID: ${process.pid}`);