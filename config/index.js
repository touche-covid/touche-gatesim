/*
* Configuration file :
* Mode :
* 	- local
* 	- staging (development)
* 	- production
* The port used depends of the mode
*/
var config = {
    local: {
        mode: 'local',
        port: 5000,
        server: 'https://covid19-touche.herokuapp.com'
    },
    production: {
        mode: 'production',
        port: 5000,
        server: 'https://covid19-touche.herokuapp.com'
    }
}
module.exports = function(mode) {
    return config[mode || process.argv[0] || 'local'] || config.local;
}
