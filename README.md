# Touché Covid GateSim

This Gate Simulator webpage is part of [our](#Team) contribution to the Covid-19 crisis from a larger solution presented at a hackathon/startupweekend - April 2020.

## Installation and Deployment

TODO: instructions remain to be written but will most likely require running a docker-compose command

## Usage

TODO: provide explanation/description

## Team

  - Andry
  - Anupam
  - Ialifinaritra
  - Nassim
  - Pierre
  - Voahary

## License

[MIT](https://choosealicense.com/licenses/mit/)
